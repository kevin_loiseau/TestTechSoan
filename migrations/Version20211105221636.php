<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211105221636 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE document_performance');
        $this->addSql('ALTER TABLE performance ADD document_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE performance ADD CONSTRAINT FK_82D79681C33F7837 FOREIGN KEY (document_id) REFERENCES document (id)');
        $this->addSql('CREATE INDEX IDX_82D79681C33F7837 ON performance (document_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE document_performance (document_id INT NOT NULL, performance_id INT NOT NULL, INDEX IDX_7EA1EBB91ADEEE (performance_id), INDEX IDX_7EA1EBC33F7837 (document_id), PRIMARY KEY(document_id, performance_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE document_performance ADD CONSTRAINT FK_7EA1EBB91ADEEE FOREIGN KEY (performance_id) REFERENCES performance (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document_performance ADD CONSTRAINT FK_7EA1EBC33F7837 FOREIGN KEY (document_id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE performance DROP FOREIGN KEY FK_82D79681C33F7837');
        $this->addSql('DROP INDEX IDX_82D79681C33F7837 ON performance');
        $this->addSql('ALTER TABLE performance DROP document_id');
    }
}
