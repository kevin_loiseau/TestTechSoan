<?php


namespace App\Service;

use App\Entity\Document;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;


use App\Entity\User;

class DocumentService
{

    /**
     * @param User $owner
     * @param array $performances
     * @param string $realPath
     * @return Document
     */
    public function newDocument(User $owner, array $performances, string $realPath): Document {
        $storagePath = $realPath."/public/storage";
        $formatedName = strtolower(trim($owner->getFirstName())."_".trim($owner->getLastName()));
        $filesystem = new Filesystem();
        $dirName = trim($storagePath.'/'.$formatedName);
        if (!$filesystem->exists($dirName)){
            $filesystem->mkdir($dirName);
        }

        $fileName = $this->checkFileName($formatedName, $dirName);
        return new Document($owner, $fileName, $dirName.'/'.$fileName, $performances);
    }

    private function checkFileName(string $fileName, string $dirname): string
    {
        $docs = scandir($dirname);
        $extention = ".pdf";
        for($i=1; in_array($fileName.$extention, $docs); ++$i) {
            $docMatch = $docs[array_search($fileName.$extention, $docs)];
            $openDoc = strrpos($docMatch, '(');
            $closeDoc = strrpos($docMatch, ')');
            if($openDoc && $closeDoc) {
                $fileName = str_replace(strval(intval(substr($docMatch, $openDoc+1, ($closeDoc-$openDoc)-1))), strval(intval(substr($docMatch, $openDoc+1, ($closeDoc-$openDoc)-1))+1), $fileName);
            }else{
                $fileName.="(1)";
            }
        }
        $fileName.=$extention;
        return $fileName;
    }
}