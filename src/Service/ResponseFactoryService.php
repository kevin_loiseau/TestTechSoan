<?php


namespace App\Service;


use App\Entity\Document;
use App\Entity\Performance;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class ResponseFactoryService
{

    /**
     * @param  array|string $toSerialize
     * @param  int $statusCode
     * @return Response
     */
    public function responseFactory(array $toSerialize, int $statusCode):Response
    {
        $response = new Response();
        $jsonMessage = json_encode($toSerialize);
        $response->setContent($jsonMessage);
        $response->setStatusCode($statusCode);
        $response->setCharset('utf-8');
        $response->headers->set("Content-Type", "application/json");
        return $response;
    }

    public function perfomanceNormalizer(Performance $performance): array
    {
            return [
                "id" => $performance->getId(),
                "name" => $performance->getName(),
                "price" => $performance->getPrice(),
                "document" => is_null($performance->getDocument()) || empty($performance->getDocument()) ? "Pas de document assosié" : $this->documentNormalizer($performance->getDocument())
            ];

    }

    public function perfomancesNormalizer(array $perfomances):array
    {
        $permoancesNormalized = [];
        foreach($perfomances as $perfomance){
            $permoancesNormalized[]=$this->perfomanceNormalizer($perfomance);
        }
        return $permoancesNormalized;
    }

    public function documentNormalizer(Document $document, string $realPath): array
    {
        return [
            "name" => $document->getName(),
            "path" => $document->getPath(),
            "owner" => $this->userNormalizer($document->getOwner(), false),
            "globalPrice"=> $document->getGlobalPrice()
        ];
    }

    public function documentsNormalizer(array $documents): array
    {
        $docuemntsNormalized = [];
        foreach($documents as $document){
            $docuemntsNormalized[]=$this->documentNormalizer($document);
        }
        return $docuemntsNormalized;
    }

    public function userNormalizer(User $user, bool $isRequired): array
    {
        $userNormalized = [
            "fistrName" => $user->getFirstName(),
            "lastName" => $user->getLastName(),
            "email" => $user->getEmail(),
            "docuement" => ""
        ];
        if ($isRequired) {
            $userNormalized["documents"] = $this->documentsNormalizer($user->getDocuments());
        } else {
            array_splice($userNormalized, 3);
        }
        return $userNormalized;
    }

}