<?php


namespace App\Controller;

use App\Entity\User;
use App\Repository\PerformanceRepository;
use App\Service\DocumentService;
use App\Service\ResponseFactoryService;
use ContainerE16xOqs\get_Console_Command_About_LazyService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;




/**
 * @Route("/document")
 */
class DocumentController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * @Route("", name="create_new_document", methods={"POST"})
     * @param Request $request
     * @param PerformanceRepository $performanceRepository
     * @param DocumentService $documentService
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function new(Request $request,
                        PerformanceRepository $performanceRepository,
                        DocumentService $documentService,
                        EntityManagerInterface $entityManager): Response
    {

        $responseFactoryService = new ResponseFactoryService();

        $data = json_decode($request->getContent(), true);
        $firstName=$data["firstName"];
        $lastName=$data["lastName"];
        $email = $data["email"];
        $performancesId = $data["performancesId"];

        if ($firstName==="" || $lastName==="" || $email==""|| sizeof($performancesId)===0){
            $toSerialize = ["Le formulaire a mal été remplis."];
            $statusCode = 403;
        }else{
            $user = new User($firstName, $lastName, $email);

            $performances = $performanceRepository->findBy(["id"=>$performancesId]);
            $realPath = $this->getParameter("kernel.project_dir");
            $document = $documentService->newDocument($user, $performances, $realPath);
            $document->setCreationDate(new \DateTime());

            $html = $this->renderView('pdf/estimate.html.twig', [
                'document' => $document,
                'globalPrice' => $document->getGlobalPrice()
            ]);

            $dompdf = new Dompdf();
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $output = $dompdf->output();
            file_put_contents($document->getPath(), $output);
            $document->setPath('/storage/'.strtolower(trim($document->getOwner()->getFirstName())."_".trim($document->getOwner()->getLastName())).'/'.$document->getName());
            $entityManager->persist($document);
            $entityManager->flush();

            $responseFactoryService = new ResponseFactoryService();
            $realPath = $this->getParameter("kernel.project_dir");

            $toSerialize = $responseFactoryService->documentNormalizer($document, $realPath);

            if(sizeof($toSerialize)>0){
                $statusCode = 201;
            }else{
                $statusCode = 401;
                $toSerialize = ["Le document n'a pas été créé"];
            }
        }

        return $responseFactoryService->responseFactory($toSerialize, $statusCode);
    }
}