<?php


namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{

    /**
     * @return Response
     * @Route("/", name="home_page", methods={"GET"})
     */
    public function index(): Response{
        return $this->render("home/home.html.twig");
    }
}