<?php


namespace App\Controller;
use App\Repository\PerformanceRepository;
use App\Service\ResponseFactoryService;
use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/performance")
 */
class PerformanceController extends AbstractController
{

    /**
     * @Route("", name="get_all_performances", methods={"GET"})
     * @param PerformanceRepository $performanceRepository
     * @return Response
     */
    public function index(PerformanceRepository $performanceRepository):Response
    {
        $performances = $performanceRepository->findAll();
        $responseFactoryService = new ResponseFactoryService();
        $performancesNormalized = $responseFactoryService->perfomancesNormalizer($performances);
        if(is_null($performancesNormalized) || empty($performancesNormalized)){
            $statusCode = 404;
        }else{
            $statusCode = 200;
        }
        return $responseFactoryService->responseFactory($performancesNormalized, $statusCode);
    }
}