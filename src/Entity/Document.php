<?php

namespace App\Entity;

use App\Repository\DocumentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use \DateTimeInterface;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity(repositoryClass=DocumentRepository::class)
 */
class Document
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="text")
     */
    private string $name;

    /**
     * @ORM\Column(type="text")
     */
    private string $path;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="documents", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private User $owner;

    /**
     * @ORM\Column(type="date")
     */
    private DateTimeInterface $creationDate;

    /**
     * @ORM\OneToMany(targetEntity=Performance::class, mappedBy="document")
     */
    private Collection $performances;




    public function __construct(User $owner, string $name, string $path, array $performances)
    {
        $this->owner = $owner;
        $this->name = $name;
        $this->path = $path;
        $this->performances = new ArrayCollection($performances);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return void
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner(User $owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreationDate(): DateTimeInterface
    {
        return $this->creationDate;
    }

    /**
     * @param DateTimeInterface $creationDate
     */
    public function setCreationDate(\DateTimeInterface $creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return Collection|Performance[]
     */
    public function getPerformances(): Collection
    {
        return $this->performances;
    }

    public function addPerformance(Performance $performance): self
    {


        return $this;
    }

    public function addPerformances(array $performances):void
    {
        foreach ($performances as $performance) {
            if (!$this->performances->contains($performance)) {
                $this->performances[] = $performance;
                $performance->setDocument($this);
            }
        }
    }

    public function removePerformance(Performance $performance): self
    {
        if ($this->performances->removeElement($performance)) {
            // set the owning side to null (unless already changed)
            if ($performance->getDocument() === $this) {
                $performance->setDocument(null);
            }
        }

        return $this;
    }

    public function getGlobalPrice():float
    {
        $prices = 0;

        foreach ($this->performances as $performance) {
            $prices += $performance->getPrice();
        }
        return $prices;
    }


}
