#TEST SOAN

#Installation : 


Se rendre à la racine du projet :
> cd TestTechSoan

Lancer la commande (cela prendra plusieur minutes au premier build) : 
>docker-compose up --build

Une fois le build terminé et les containers lancés, ouvrir un nouveau terminal et tapper :
>docker exec -it -u root soan_container bash

Cette commande ouvre un terminal à l'intérieur du container de notre application.
Il ne reste plus qu'à exécuter les commandes suivantes : 
>composer install

>php bin/console doctrine:migrations:migrate

>php bin/console doctrine:fixtures:load

En se rendant sur localhost:81 on se trouve sur la page d'accueil, preuve que le projet est lancé.

Les routes : 

/document : POST - pour créer un nouveau PDF, objet JSON :

>{
    "firstName": "string",
    "lastName": "string",
    "email": "string",
    "performancesId": [int,int]
}

/performance : GET - pour récupérer les prestations
