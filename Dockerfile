FROM php:7.4-apache

#set params
ARG BUILD_ARGUMENT_ENV=dev
ARG UID=1000
ARG GID=1000
ENV APP_HOME /var/www/html
ENV USERNAME=www-data

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    procps \
    nano \
    default-mysql-client \
    git \
    unzip \
    libpng-dev \
    libicu-dev \
    zlib1g-dev \
    libxml2 \
    libxml2-dev \
    libreadline-dev \
    supervisor \
    cron \
    sudo \
    libzip-dev \
    wget \
    librabbitmq-dev \
    gnupg2 \
    && pecl install amqp \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-configure intl \
    && docker-php-ext-install \
    gd \
    mysqli \
    pdo_mysql \
    sockets \
    intl \
    opcache \
    zip \
    && docker-php-ext-enable amqp \
    && rm -rf /tmp/* \
    && rm -rf /var/list/apt/* \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

# disable default site and delete all default files inside APP_HOME
RUN a2dissite 000-default.conf
RUN rm -r $APP_HOME

# create document root, fix permissions for www-data user and change owner to www-data
RUN mkdir -p $APP_HOME/public \
    && mkdir -p /home/$USERNAME \
    && chown $USERNAME:$USERNAME /home/$USERNAME \
    && mkdir -p $APP_HOME/soan \
    && usermod -u $UID $USERNAME -d /home/$USERNAME \
    && groupmod -g $GID $USERNAME \
    && chown -R ${USERNAME}:${USERNAME} $APP_HOME

# put apache and php config for Symfony, enable sites
COPY ./Docker/symfony.conf /etc/apache2/sites-available/symfony.conf
RUN a2ensite symfony.conf
COPY ./Docker/$BUILD_ARGUMENT_ENV/php.ini /usr/local/etc/php/php.ini

# install composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN chmod +x /usr/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER 1

WORKDIR $APP_HOME/soan/

RUN mkdir "var"

COPY ./ ./

RUN rm -rf var/cache/dev \
    && rm -rf var/log/

RUN sudo chown -R ${USERNAME}:${USERNAME} $APP_HOME/soan/var/ \
    && chmod -R ug+rwX var/

RUN chmod -R 777 $APP_HOME/soan/var/

EXPOSE 80